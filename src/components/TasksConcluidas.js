import React, { useEffect, useState } from 'react';
import { View, StyleSheet, Alert, ScrollView } from 'react-native';
import { Text } from 'react-native-paper';
import CreateTarefa from './CreateTask/CreateTask';
import { supabase } from '../utils/supabase';
import DeleteTask from './DeleteTasks/DeleteTask';
import EditTask from './EditTask/EditTask';
import AtiveTask from './AtiveTarefa';

const TasksConcluidas = () => {
  const [tarefas, setTarefas] = useState([]);

  useEffect(() => {
    const intervalId = setInterval(PullTasks, 5000);
  }, []);

  async function PullTasks() {
    console.log('vai iniciar o try');
    try {
      const { data: tasks, error: fetchError } = await supabase
        .from('tarefa')
        .select('*')
        .eq('status', false);

      if (fetchError) {
        throw fetchError;
      }

      if (!tasks || tasks.length === 0) {
        Alert.alert('Atenção', 'Nenhuma tarefa foi encontrada no banco de dados.');
        return;
      }

      setTarefas(tasks.map((task, index) => (
        <View key={index} style={styles.wrapperTarefa}>
          <Text style={styles.textoTask}>{task.name}</Text>
          <AtiveTask />
          <DeleteTask />
          <EditTask />
        </View>
      )));

    } catch (error) {
      if (error instanceof Error) {
        Alert.alert('Erro:', error.message);
      }
    }
  }

  return (
    <ScrollView contentContainerStyle={styles.scrollViewContent}>
      <View style={styles.container}>
        <CreateTarefa />
        <View style={styles.wrapper}>
          <Text style={styles.texto}>Tarefas concluidas!</Text>
          {tarefas}
        </View>
      </View>
    </ScrollView>
  );
};

export default TasksConcluidas;

const styles = StyleSheet.create({
  scrollViewContent: {
    flexGrow: 1,
  },
  container: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
    margin: 10,
  },
  wrapper: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    marginVertical: 30,
  },
  wrapperTarefa: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    marginVertical: 10,
  },
  forInput: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
    marginBottom: 10,
  },
  texto: {
    fontSize: 20,
    color: 'black',
    fontFamily: 'Arial',
    fontWeight: 'bold',
    textAlign: 'center',
  },
  textoTask: {
    fontSize: 16,
    color: 'black',
    fontFamily: 'Arial',
    textAlign: 'center',
  },
  tarefa: {
    marginBottom: 10,
  },
});
