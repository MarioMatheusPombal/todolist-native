import * as React from 'react';
import { View, StyleSheet } from 'react-native';
import { BottomNavigation, Text } from 'react-native-paper';
import MeuScanner from './meuScanner';
import ActiveTasks from './ActiveTasks/ActiveTasks';
import CompletedTasks from './CompletedTasks/CompletedTasks';

const ActiveTasksContainer = () => <View style={styles.container}>
  <ActiveTasks />
</View>;

const QRoute = () => <View style={styles.container}>
  <MeuScanner />
</View>;

const CompletedTasksContainer = () => <View style={styles.container}>
  <CompletedTasks />
</View>;

const BottomNavBar = () => {
  const [index, setIndex] = React.useState(0);
  const [routes] = React.useState([
    { key: 'tarefas', title: 'Tarefas', focusedIcon: 'heart', unfocusedIcon: 'heart-outline' },
    { key: 'tarefasconcluidas', title: 'Concluidas', focusedIcon: 'archive' },
    { key: 'qrcode', title: 'QRCode', focusedIcon: 'qrcode' },
    // Adicione a nova rota ao BottomNavigation
  ]);

  const renderScene = BottomNavigation.SceneMap({
    tarefas: ActiveTasksContainer,
    tarefasconcluidas: CompletedTasksContainer,
    qrcode: QRoute
    // Mapeie a nova rota para o componente correspondente
  });

  return (
    <BottomNavigation
      navigationState={{ index, routes }}
      onIndexChange={setIndex}
      renderScene={renderScene}
    />
  );
};

export default BottomNavBar;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
  },
});
