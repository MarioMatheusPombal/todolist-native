import React from 'react';
import { Pressable, StyleSheet } from 'react-native';
import { IconButton } from 'react-native-paper';

const DesativeTask = () => {

  return (
    <Pressable style={styles.button} onPress={() => { }}>
      <IconButton icon="check-circle-outline" color="red" size={23} />
    </Pressable>
  );
}

const styles = StyleSheet.create({
  button: {
    backgroundColor: 'transparent',
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 999,
    padding: 0,
    marginVertical: 2,
  },
});

export default DesativeTask;
