import React from 'react';
import { Pressable, StyleSheet } from 'react-native';
import { IconButton } from 'react-native-paper';

const AtiveTask = () => {

  return (
    <Pressable style={styles.button} onPress={() => { }}>
      <IconButton icon="arrow-left-bold-circle-outline" color="red" size={23} />
    </Pressable>
  );
}

const styles = StyleSheet.create({
  button: {
    backgroundColor: 'transparent',
    alignItems: 'center',
    justifyContent: 'center',
    padding: 0,
    marginVertical: 2,
    marginLeft: 10
  },
});

export default AtiveTask;
