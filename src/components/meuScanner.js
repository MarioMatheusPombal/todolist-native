import { CameraView, useCameraPermissions } from 'expo-camera/next';
import * as MediaLibrary from 'expo-media-library';
import { useState, useRef } from 'react';
import { Alert, Button, Modal, StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import { MaterialIcons } from '@expo/vector-icons';
import { createTask } from '../utils/api';

export default function MeuScanner() {
  const [torch, setTorch] = useState(false);
  const [facing, setFacing] = useState('back');
  const [permission, requestPermission] = useCameraPermissions();
  const [modalVisible, setModalVisible] = useState(false);
  const [qrData, setQrData] = useState(null);
  const cameraRef = useRef(null);

  if (!permission) {
    return <View />;
  }

  if (!permission.granted) {
    return (
      <View style={styles.container}>
        <Text style={{ textAlign: 'center' }}>Precisa da sua permissão para usar camera</Text>
        <Button onPress={requestPermission} title="grant permission" />
      </View>
    );
  }

  async function insertData(dataList) {
    for (const data of dataList) {
      try {
        const task = await createTask(data); 
        Alert.alert(`Tarefa '${task.name}' criada com sucesso!`);
      } catch (error) {
        Alert.alert('Erro ao criar tarefa:', error.message);
      }
      console.log('Inserindo dados:', data);
    }
  };


  const onBarcodeScanned = (barcode) => {
    const data = barcode.data.split(';');
    setQrData(data);
    setModalVisible(true);
  };

  const onConfirmInsert = () => {
    insertData(qrData);
    setModalVisible(false);
  };

  function toggleTorch() {
    setTorch(current => (current === false ? true : false));
  }

  function toggleCameraFacing() {
    setFacing(current => (current === 'back' ? 'front' : 'back'));
  }

  const takePicture = async () => {
    if (cameraRef.current) {
      const photo = await cameraRef.current.takePictureAsync();
      onPictureSaved(photo);
    }
  };

  const onPictureSaved = async photo => {
    console.log('photo', photo);
    const asset = await MediaLibrary.createAssetAsync(photo.uri);
    await MediaLibrary.createAlbumAsync('MeuScanner', asset, false);
  };

  return (
    <View style={styles.container}>
      <CameraView
        ref={cameraRef}
        style={styles.camera}
        facing={facing}
        onBarcodeScanned={onBarcodeScanned}
        barcodeScannerSettings={{
          barcodeTypes: ["qr"],
        }}

        enableTorch={torch}
      >

        <View style={styles.buttonContainer}>
          <TouchableOpacity style={styles.button} onPress={toggleCameraFacing}>
            <MaterialIcons name="flip-camera-android" size={24} color="white" />
          </TouchableOpacity>
          <TouchableOpacity style={styles.button} onPress={takePicture}>
            <MaterialIcons name="camera" size={24} color="white" />
          </TouchableOpacity>
          <TouchableOpacity style={styles.button} onPress={toggleTorch}>
            <MaterialIcons name="flash-on" size={24} color="white" />
          </TouchableOpacity>
        </View>
      </CameraView>
      <Modal
        animationType="slide"
        transparent={true}
        visible={modalVisible}
        onRequestClose={() => {
          Alert.alert("Modal foi fechado.");
          setModalVisible(!modalVisible);
        }}
      >
        <View style={styles.centeredView}>
          <View style={styles.modalView}>
            <Text style={styles.modalText}>Deseja inserir os seguintes dados?</Text>
            <Text style={styles.modalText}>{qrData && qrData.join(', ')}</Text>

            <TouchableOpacity
              style={[styles.modalbutton,styles.buttonClose]}
              onPress={onConfirmInsert}
            >
              <Text style={styles.textStyle}>Sim</Text>
            </TouchableOpacity>
          </View>
        </View>
      </Modal>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
  },
  camera: {
    flex: 1,
  },
  buttonContainer: {
    flex: 1,
    flexDirection: 'row',
    backgroundColor: 'transparent',
    margin: 64,
  },
  button: {
    flex: 1,
    alignSelf: 'flex-end',
    alignItems: 'center',
  },
  modalbutton: {
    padding: 5,
    borderRadius: 5,
  },
  text: {
    fontSize: 24,
    fontWeight: 'bold',
    color: 'white',
  },
  centeredView: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    marginTop: 22
  },
  modalView: {
    margin: 20,
    backgroundColor: "white",
    borderRadius: 20,
    padding: 35,
    alignItems: "center",
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2
    },
    shadowOpacity: 0.25,
    shadowRadius: 4,
    elevation: 5
  },
  buttonClose: {
    backgroundColor: "green",
  },
  textStyle: {
    color: "white",
    fontWeight: "bold",
    textAlign: "center"
  },
  modalText: {
    marginBottom: 15,
    textAlign: "center"
  }
});
