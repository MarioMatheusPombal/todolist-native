import React from 'react';
import { StyleSheet, Pressable, Alert } from 'react-native';
import { deleteTask } from '../../utils/api';
import { IconButton } from 'react-native-paper';

const DeleteTask = ({ taskId }) => {
  async function handleDeleteTask() {
    try {
      await deleteTask(taskId);
      Alert.alert(`Tarefa deletada com sucesso!`);
    } catch (error) {
      Alert.alert('Erro ao deletar tarefa:', error.message);
    }
  }

  return (
    <Pressable style={styles.button} onPress={handleDeleteTask}>
      <IconButton icon="delete" color="red" size={20} />
    </Pressable>
  );
}

const styles = StyleSheet.create({
  button: {
    backgroundColor: 'transparent',
    alignItems: 'center',
    justifyContent: 'center',
    padding: 0,
    marginVertical: 2,
  },
});

export default DeleteTask;
