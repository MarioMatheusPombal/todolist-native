import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
    container: {
      flexDirection: 'row',
      alignItems: 'center',
      justifyContent: 'space-between',
      paddingHorizontal: 10,
      paddingVertical: 5,
      marginTop: 50,
      marginHorizontal: 5,
    },
    input: {
      flex: 1,
      height: 40,
      borderWidth: 1,
      borderColor: 'black',
      paddingHorizontal: 10,
      borderRadius: 15,
      marginRight: 5,
    },
    button: {
      height: 40,
      paddingHorizontal: 20,
      borderRadius: 15,
      backgroundColor: '#007AFF',
      justifyContent: 'center',
    },
    buttonText: {
      color: 'white',
      fontSize: 16,
      textAlign: 'center',
    }
  });
  

export default styles;
