import React, { useState } from 'react';
import { View, TextInput, Pressable, Text, Alert } from 'react-native';
import { createTask } from '../../utils/api';
import styles from './styles';

const CreateTask = () => {
  const [name, setName] = useState('');

  async function handleCreateTask() {
    try {
      const task = await createTask(name); 
      Alert.alert(`Tarefa '${task.name}' criada com sucesso!`);
      setName('');
    } catch (error) {
      Alert.alert('Erro ao criar tarefa:', error.message);
    }
  }

  return (
    <View style={styles.container}>
      <TextInput
        style={styles.input}
        value={name}
        onChangeText={setName}
        placeholder="Nome da tarefa"
      />
      <Pressable
        style={styles.button}
        onPress={handleCreateTask}
      >
        <Text style={styles.buttonText}>Enviar</Text>
      </Pressable>
    </View>
  );
}

export default CreateTask
