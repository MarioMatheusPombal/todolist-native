import React from 'react';
import { Pressable } from 'react-native';
import { IconButton } from 'react-native-paper';
import styles from './styles';

const EditTask = ({ onPress })  => {
  return (
    <Pressable style={styles.button} onPress={onPress}>
      <IconButton icon="border-color" color="red" size={20} />
    </Pressable>
  );
}

export default EditTask;
