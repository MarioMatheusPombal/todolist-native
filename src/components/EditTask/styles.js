import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
    button: {
        backgroundColor: 'transparent',
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 999,
        padding: 0,
        marginVertical: 2,
        marginTop: 1
    },
});

export default styles;
