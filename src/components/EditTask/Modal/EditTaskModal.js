import React, { useState, useEffect } from 'react';
import { Modal, Text, View, TextInput, Button } from 'react-native';
import { updateTask } from '../../../utils/api';
import styles from './styles';

const EditTaskModal = ({ visible, onClose, task, onUpdate }) => {
  const [newTaskName, setNewTaskName] = useState(''); 
  const [isDisabled, setIsDisabled] = useState(true); 

  useEffect(() => {
    if (task) {
      setNewTaskName(task.name);
    }
  }, [task]);

  useEffect(() => {
    setIsDisabled(newTaskName.trim() === '');
  }, [newTaskName]);

  const handleConfirm = async () => {
    try {
      if (!task) {
        console.error('Tarefa veio nula...');
        return;
      }
      
      await updateTask(task.id, newTaskName);
      onUpdate();
      onClose();
    } catch (error) {
      console.error('Erro ao atualizar a tarefa:', error.message);
    }
  };

  return (
    <Modal
      animationType="fade"
      transparent={true}
      visible={visible}
      onRequestClose={onClose}
    >
      <View style={styles.modalContainer}>
        <View style={styles.modalContent}>
          <Text style={styles.title}>Editar tarefa</Text>
          <TextInput
            style={styles.input}
            value={newTaskName}
            onChangeText={setNewTaskName}
            placeholder="Digite o novo título da tarefa"
          />
          <View style={styles.buttonContainer}>
            <Button title="Cancelar" onPress={onClose} />
            <Button title="Confirmar" onPress={handleConfirm} disabled={isDisabled}>
              <Text>Confirmar</Text>
            </Button>
          </View>
        </View>
      </View>
    </Modal>
  );
};

export default EditTaskModal;
