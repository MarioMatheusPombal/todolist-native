import React, { useEffect, useState } from 'react';
import { View, Alert, ScrollView } from 'react-native';
import { Text } from 'react-native-paper';
import DeleteTask from '../DeleteTasks/DeleteTask';
import AtiveTask from '../AtiveTarefa';
import { fetchCompletedTasks } from '../../utils/api';
import styles from './styles';

const CompletedTasks = () => {
  const [tasks, setTasks] = useState([]);

  useEffect(() => {
    const intervalId = setInterval(PullTasks, 5000);
  }, []);

  async function PullTasks() {
    // console.log('Recuperando tarefas...');
    try {
      const tasks = await fetchCompletedTasks();

      if (!tasks || tasks.length === 0) {
        Alert.alert('Atenção', 'Nenhuma tarefa foi encontrada no banco de dados.');
        return;
      }

      setTasks(tasks);
      // console.log('Tarefas ativas encontradas:', tasks);
    } catch (error) {
      if (error instanceof Error) {
        Alert.alert('Erro:', error.message);
      }
    }
  }

  return (
    <ScrollView>
      <View style={styles.container}>
        <View style={styles.wrapper}>
          <Text style={styles.text}>Tarefas concluídas com sucesso!</Text>
          {tasks.map((task, index) => (
            <View key={index} style={styles.wrapperTask}>
              <Text style={styles.textTask}>{task.name}</Text>
              <AtiveTask />
              <DeleteTask taskId={task.id} />
            </View>
          ))}
        </View>
      </View>
    </ScrollView>
  );
};

export default CompletedTasks;
