import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
    margin: 10,
  },
  wrapper: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    marginVertical: 30,
  },
  wrapperTask: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  forInput: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
    marginBottom: 10,
  },
  text: {
    fontSize: 20,
    color: 'black',
    fontFamily: 'Arial',
    fontWeight: 'bold',
    textAlign: 'center',
    marginVertical: 40
  },
  textName: {
    fontSize: 16,
    color: 'black',
    fontFamily: 'Arial',
    textAlign: 'center',
  },
  task: {
    marginBottom: 10,
  },
});

export default styles;
