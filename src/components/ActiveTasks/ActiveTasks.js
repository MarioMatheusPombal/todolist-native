import React, { useEffect, useState } from 'react';
import { View, Alert, ScrollView } from 'react-native';
import { Text } from 'react-native-paper';
import CreateTask from '../CreateTask/CreateTask';
import DeleteTask from '../DeleteTasks/DeleteTask';
import EditTask from '../EditTask/EditTask';
import DesativeTask from '../DesativeTarefa';
import EditTaskModal from '../EditTask/Modal/EditTaskModal';
import { updateTask, fetchActiveTasks } from '../../utils/api';
import styles from './styles';

const ActiveTasks = () => {
  const [tasks, setTasks] = useState([]);
  const [modalVisible, setModalVisible] = useState(false);
  const [selectedTask, setSelectedTask] = useState(null);

  useEffect(() => {
    const intervalId = setInterval(PullTasks, 5000);
  }, []);

  async function PullTasks() {
    // console.log('Recuperando tarefas...');
    try {
      const tasks = await fetchActiveTasks();

      if (!tasks || tasks.length === 0) {
        Alert.alert('Atenção', 'Nenhuma tarefa foi encontrada no banco de dados.');
        return;
      }

      setTasks(tasks);
      // console.log('Tarefas ativas encontradas:', tasks);
    } catch (error) {
      if (error instanceof Error) {
        Alert.alert('Erro:', error.message);
      }
    }
  }

  const handleEdit = (task) => {
    console.log("Editando tarefa:", task);
    setSelectedTask(task);
    setModalVisible(true);
  };

  const handleModalClose = () => {
    setModalVisible(false);
  };

  const handleTaskUpdate = async (newTaskName) => {
    try {
      if (selectedTask && newTaskName !== '') {
        await updateTask(selectedTask.id, newTaskName);
        PullTasks();
        setModalVisible(false);
      }
    } catch (error) {
      console.error('Erro ao atualizar a tarefa:', error.message);
    }
  };

  return (
    <ScrollView>
      <View style={styles.container}>
        <CreateTask />
        <View style={styles.wrapper}>
          <Text style={styles.text}>Tarefas ativas!</Text>
          {tasks.map((task, index) => (
            <View key={index} style={styles.wrapperTask}>
              <Text style={styles.textTask}>{task.name}</Text>
              <DesativeTask />
              <DeleteTask taskId={task.id} />
              <EditTask onPress={() => handleEdit(task)} />
            </View>
          ))}
        </View>
        <EditTaskModal
          visible={modalVisible}
          onClose={handleModalClose}
          task={selectedTask}
          onUpdate={handleTaskUpdate}
        />
      </View>
    </ScrollView>
  );
};

export default ActiveTasks;
