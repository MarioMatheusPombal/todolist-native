import 'react-native-url-polyfill/auto';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { createClient } from '@supabase/supabase-js';

const supabaseUrl = 'https://yevfsfhyrikxpfipwzwb.supabase.co';
const supabaseAnonKey = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJzdXBhYmFzZSIsInJlZiI6InlldmZzZmh5cmlreHBmaXB3endiIiwicm9sZSI6ImFub24iLCJpYXQiOjE3MTI4NDA0NzQsImV4cCI6MjAyODQxNjQ3NH0.aD5ys2sVP9M9rURQRY78IXCZx-wenkfywZouDIZ0dVw';

export const supabase = createClient(supabaseUrl, supabaseAnonKey, {
  auth: {
    storage: AsyncStorage,
    autoRefreshToken: true,
    persistSession: true,
    detectSessionInUrl: false,
  },
});
