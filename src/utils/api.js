import { supabase } from './supabase';

export async function createTask(name) {
    try {
        if (!name) {
            throw new Error('Por favor, insira o nome da tarefa.');
        }

        const { error: insertError } = await supabase.from('tarefa').insert([{ name }]);
        if (insertError) {
            throw insertError;
        }

        const { data: tasks, error: fetchError } = await supabase
            .from('tarefa')
            .select('*')
            .eq('name', name);

        if (fetchError) {
            throw fetchError;
        }

        if (!tasks || tasks.length === 0) {
            throw new Error('A tarefa não foi encontrada no banco de dados.');
        }

        return tasks[0];
    } catch (error) {
        throw error;
    }
}

export async function updateTask(id, newName) {
    try {
        const { data, error } = await supabase
            .from('tarefa')
            .update({ name: newName })
            .eq('id', id);

        if (error) {
            throw error;
        }

        console.log('Tarefa atualizada:', data);
        return data;
    } catch (error) {
        console.error('Erro ao atualizar a tarefa:', error.message);
        throw error;
    }
}

export async function deleteTask(id) {
    try {
        const { data, error } = await supabase
            .from('tarefa')
            .delete('*')
            .eq('id', id);

        if (error) {
            throw error;
        }

        console.log('Tarefa deletada:', data);
        return data;
    } catch (error) {
        console.error('Erro ao deletar a tarefa:', error.message);
        throw error;
    }
}

export async function fetchActiveTasks() {
    try {
        const { data: tasks, error: fetchError } = await supabase
            .from('tarefa')
            .select('*')
            .eq('status', true);

        if (fetchError) {
            throw fetchError;
        }

        return tasks;
    } catch (error) {
        throw error;
    }
}

export async function fetchCompletedTasks() {
    try {
        const { data: tasks, error: fetchError } = await supabase
            .from('tarefa')
            .select('*')
            .eq('status', false);

        if (fetchError) {
            throw fetchError;
        }

        return tasks;
    } catch (error) {
        throw error;
    }
}
